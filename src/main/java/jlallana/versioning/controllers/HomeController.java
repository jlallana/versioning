package jlallana.versioning.controllers;

import jlallana.versioning.entities.ProductRegistrationEntity;
import jlallana.versioning.services.ProductService;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeController {

    private ProductService productService;

    public HomeController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping(path="/")
    public Iterable<ProductRegistrationEntity> index() {
        return this.productService.list();
    }
}