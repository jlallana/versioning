package jlallana.versioning.repositories;

import jlallana.versioning.entities.ProductRegistrationEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

public interface ProductRegistrationRepository  extends CrudRepository<ProductRegistrationEntity, Integer> {
}