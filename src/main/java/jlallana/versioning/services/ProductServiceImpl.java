package jlallana.versioning.services;

import jlallana.versioning.entities.ProductRegistrationEntity;
import jlallana.versioning.repositories.ProductRegistrationRepository;
import org.springframework.stereotype.Service;

@Service
public class ProductServiceImpl implements ProductService {

    private ProductRegistrationRepository productRegistrationRepository;

    public ProductServiceImpl(ProductRegistrationRepository productRegistrationRepository) {
        this.productRegistrationRepository = productRegistrationRepository;
    }

    @Override
    public Iterable<ProductRegistrationEntity> list() {
        return this.productRegistrationRepository.findAll();
    }

    @Override
    public void demo() {
        ProductRegistrationEntity pr = new ProductRegistrationEntity();
        pr.setName("Test");
        this.productRegistrationRepository.save(pr);
    }
}
