package jlallana.versioning.services;

import jlallana.versioning.entities.ProductRegistrationEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ProductService {
    Iterable<ProductRegistrationEntity> list();

    void demo();
}
